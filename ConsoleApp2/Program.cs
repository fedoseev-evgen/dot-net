﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace ConsoleApp2
{
    public delegate bool IsMoreZero(double a);
    
    public static class LINQExtension
    {
        public static double MySum(this IEnumerable<double> source)
        {
            return source.Sum();
        }
        
        public static bool IsInt(this IEnumerable<char> source)
        {
            var output = source.Where(c => '0' <= c && c <= '9');
            if (output.Count() == source.Count())
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static IEnumerable<double> FindMoreZero(this IEnumerable<double> source)
        {
            return source.Where(c => 0 <= c);
        }

        public static IEnumerable<double> FindMoreZero(this IEnumerable<double> source, IsMoreZero _asd)
        {
            return source.Where(item => _asd(item));
        }

        public static IEnumerable<double> FindMoreZeroLINQ(this IEnumerable<double> source)
        {
            return from item in source
                where 0 <= item
                select item;
        }
        public static void Print(this IEnumerable<double> source)
        {
            foreach (var num in source)
            {
                Console.Write(num + " ");
            }
            Console.WriteLine("");
        }
        
    }
    
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Введите целое положительное число, а я вас проверю: ");
            string str = Console.ReadLine();
            if (str.IsInt())
            {
                Console.WriteLine("Какой ты молодец, все написал правильно!");
            }
            else
            {
                Console.WriteLine("Какой ужас, ты не смог ввести положительное целоке число!");
            }
            
            Console.WriteLine("Сейчас будет написан простой масив для того, чтобы убедиться в работе всех методов");
            
            IsMoreZero Handler = delegate(double a)
            {
                return a >= 0;
            };
            
            double[] numbers1 = {-1, 2, -3, 4};
            Console.WriteLine("Исходный массив:");
            numbers1.Print();
            Console.WriteLine("Сумма всех элементов массива: " + numbers1.Sum());
            Console.WriteLine("Массив метода, реализующего поиск напрямую:");
            numbers1.FindMoreZero().Print();
            Console.WriteLine("Массив метода, которому условие поиска передается через делегат:");
            numbers1.FindMoreZero(MyIsMoreZero).Print();
            Console.WriteLine("Массив метода, которому условие поиска передается через делегат в виде анонимного метода:");
            numbers1.FindMoreZero(Handler).Print();
            Console.WriteLine("Массив метода, которому условие поиска передается через делегат в виде лямбда-выражения:");
            numbers1.FindMoreZero(item => item >= 0).Print();
            Console.WriteLine("Массив с LINQ-выражением:");
            numbers1.FindMoreZeroLINQ().Print();
            
            double[] numbers2 = new double[100000];
            int startNumber = -995;
            for (int i = 0; i < 100000; i++)
            {
                numbers2[i] = startNumber++;
            }
            
            Stopwatch time = Stopwatch.StartNew();
            var result = numbers2.FindMoreZero();
            time.Stop();
            Console.WriteLine("Скорость метода, реализующего поиск напрямую: " + time.ElapsedTicks);
            
            time.Restart();
            numbers2.FindMoreZero(MyIsMoreZero);
            time.Stop();
            Console.WriteLine("Скорость метода, которому условие поиска передается через делегат: " + time.ElapsedTicks);
            
            time.Restart();
            numbers2.FindMoreZero(Handler);
            time.Stop();
            Console.WriteLine("Скорость метода, которому условие поиска передается через делегат в виде анонимного метода: " + time.ElapsedTicks);
            
            time.Restart();
            numbers2.FindMoreZero(item => item >= 0);
            time.Stop();
            Console.WriteLine("Скорость метода, которому условие поиска передается через делегат в виде лямбда-выражения: " + time.ElapsedTicks);
            
            time.Restart();
            numbers2.FindMoreZeroLINQ();
            time.Stop();
            
            Console.WriteLine("Скорость поиска с LINQ-выражением: " + time.ElapsedTicks);
        }

        private static bool MyIsMoreZero(double a)
        {
            return a >= 0;
        }
        
    }
}